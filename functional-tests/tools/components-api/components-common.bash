#!/usr/bin/env bash

_components_setup() {
    load "../../global-common"
    _global_setup
}

_components_teardown() {
    _global_teardown
}
